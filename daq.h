
#ifndef DAQ_H
#define DAQ_H

//HEADERS
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "uart.h"

//DEFINITIONS
#define SprintIR  4    //co2 sensor
#define COAF      1    //co sensor
#define LOX_O2    2    //o2

//comands
#define Rep_Dev_ID      "Y\r\n"
#define Pulling_Mode    "K 2\r\n"
#define OX_P_Mode       "M 1\r\n"
#define FILnUNFIL       "M 6\r\n"
#define unf_gas_con     "z\r\n"
#define fil_gas_con     "Z\r\n"
#define Temperature     "T\r\n"
#define get_readigns    "Q\r\n"
#define percent_oxigen  "%\r\n"
#define ppm_oxigen      "O\r\n"
#define M_zZTHBD        "M 14406\r\n"
#define M_zZTHB         "M 12358\r\n"
#define Readings_OX     "A\r\n"

//FUNCTIONS
int sensConf(unsigned char uartNumber, int baudRate, unsigned char mode[], unsigned char response[], int tries);
int measure(unsigned char uartNumber, unsigned char command[], int multiplier, int excess);
char *getMeasures(char src[], char fval, int nchar);
int str2int(unsigned char *ptr);
int DAQ(int t_hrs, int sp_s);

#endif

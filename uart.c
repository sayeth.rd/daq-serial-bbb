/*
  comments
*/
//libraries
#include "uart.h"

//file paths and global variables
#define incomplet_uart_path "/dev/ttyO"
struct termios options;             //The termios structure

//    *** FUNCTIONS ***

int uartConf(unsigned char uartNumber, int baudRate)
{
    unsigned char fullFileName[11];

    //completing uart file path
    if ((uartNumber==1)||(uartNumber==2)||(uartNumber==4)||(uartNumber==5))
      sprintf(fullFileName, incomplet_uart_path "%d", uartNumber);
    else{
      perror("Wrong UART number. " \
      "UART numbers availables 1, 2, 4 or 5.\n");
      return UART_NUMBER_INCORRECT;
    }
    //openign uart file
    printf("Configuring UART%d.\n", uartNumber);
    if ((uartFile[uartNumber] = open(fullFileName, O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK))<0){
    //if ((uartFile[uartNumber] = open(fullFileName, O_RDWR | O_NOCTTY | O_NDELAY))<0){
    //if ((uartFile[uartNumber] = open(fullFileName, O_RDWR | O_NOCTTY))<0){
        perror("UART: Failed to open the file.\n");
        return -1;
    }
    //Sets the parameters associated with file
    tcgetattr(uartFile[uartNumber], &options);
    //cleaning flags
    options.c_ispeed = 0;
    options.c_lflag = 0;
    options.c_line = 0;
    options.c_oflag = 0;
    options.c_ospeed = 0;
    // Set up the communications options:
    // 8-bit, enable receiver, no modem control lines
    options.c_cflag = baudRate | CS8 | CREAD | CLOCAL;
    options.c_iflag = IGNPAR;    //ignore partity errors, CR -> newline
    //options.c_iflag = IGNPAR | ICRNL;    //ignore partity errors, CR -> newline
    //options.c_lflag = ICANON;
    tcflush(uartFile[uartNumber], TCIOFLUSH);             //discard file information not transmitted
    tcsetattr(uartFile[uartNumber], TCSANOW, &options);  //changes occur immmediately
    printf("UART%d configurated.\n", uartNumber);

    return UART_FUNCTION_SUCCESSFUL;
}

int uartClose(unsigned char uartNumber)
{
    printf("Closing UART%d.\n", uartNumber);
    close(uartFile[uartNumber]);
    return UART_FUNCTION_SUCCESSFUL;
}

int uartTransmit(unsigned char uartNumber, unsigned char message[])
{
   int count;

   //writing file
   if ((count = write(uartFile[uartNumber], message, (strlen(message))))<0){        //send the string
      perror("Failed to write to the output\n");
      return -1;
   }
   tcflush(uartFile[uartNumber], TCOFLUSH);

   return UART_FUNCTION_SUCCESSFUL;
}

int uartReceive(unsigned char uartNumber)
{
    int count;

    /*if ((count = read(uartFile[uartNumber], (void*)receive[uartNumber], 100))<0){   //receive[uartNumber] the data
        perror("Failed to read from the input\n");
        return -1;
    }*/
    while((count = read(uartFile[uartNumber], (void*)receive[uartNumber], 100)) < 0);
    tcflush(uartFile[uartNumber], TCIFLUSH);

    return count;
}

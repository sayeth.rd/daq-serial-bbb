/*
*/
#include "daq.h"

//DEFINITIOS
#define data_file_path  "./DATA/data.dat"

int sensConf(unsigned char uartNumber, int baudRate, unsigned char mode[], unsigned char response[], int tries)
{
    int count;

    uartConf(uartNumber, baudRate);
    while(tries){
        uartTransmit(uartNumber, mode);
        tcdrain(uartFile[uartNumber]);  //wait all data has been sent
        printf("Command sended.\n");
        //usleep(100000);                  //give the sensor a chance to respond
        count = uartReceive(uartNumber);
        if (count == 0) printf("There was no data available to read!\n");
        else if (strcmp(receive[uartNumber], response) == 0) {
                printf("Sensor configurated.\n");
                return 0;
        } else {
            printf("The following was read in [%d]: %s\n",count,receive[uartNumber]);
            //char *c = receive[uartNumber];
            //while (*c != '\0'){
            //    printf("%d = '%c'\n",*c, *c);
            //    c++;
            //}
        }
        tries --;
    }
    printf("Sensor configuration failed.\n");
    return -1;
}

int DAQ(int t_hrs, int sp_s)
{
  FILE* dfp;                                    // create a file pointer
  const char data_header[200] =
      "t\tCO2\tCO2 f\tCO\tCO f\tO2\tO2\tT CO\tT O2\tP CO\tP O2\tRH\n"\
      "s\tppm\tppm\tppm\tppm\tppO2\t%\t°C*10\t°C\tmBar\tmBar\t%";
  char co2_uf[10]="",   co2_f[10]="",
                co_uf[10]="",    co_f[10]="",
                o2_ppm[10]="",   o2_xcent[10]="",
                co_temp[10]="",  o2_temp[10]="",
                co_press[10]="", o2_press[10]="",
                co_relH[10]="",  DATA[200]="";    //measurements varirables
  time_t curtime;                               //current time (date)
  clock_t start_t, end_t;                       //processing time measurements variables
  time_t next_samp_time, t0;                    //time control variables
	double iteration_time_ms = sp_s*1e6 - 0.2e6;  //cicle iteration maximum time
	double inactivity_time = 1;                   //time to sleep

  time(&curtime);                               //saving date in curtime
  //registro datos de inicio
  dfp = fopen(data_file_path, "w");             // open file for writing
  // send the value to the file
  fprintf(dfp, "Starting DAQ at %s\n", ctime(&curtime));
  fclose(dfp);                                  // close the file using the file pointer
  printf("%s", colums);                         //display variables colums
  //ciclo
  next_samp_time = time(NULL)+1;              //setting next sampling time
  t0 = next_samp_time;                          //saving initial time
  for(time_t t = 0; t < (t_hrs*3600); t+=sp_s){    //cycle from 0 to adquisition time (seconds), incrementing sampling period
    //checking inactivity time
    if (inactivity_time <= 1){
      usleep((int)inactivity_time);             //inactivity
    }
    else{
      printf("Ejecution time exceded.\n%s", colums);
      next_samp_time = time(NULL)+1;              //updating time values
      t = next_samp_time-t0;
    }
    while(next_samp_time != time(NULL));      //synchronizing/waiting to start measurements

    start_t = clock();                          //saving start time
      uartTransmit(COAF, get_readigns);         //transmiting commands to sensors
      uartTransmit(LOX_O2, Readings_OX);
      uartTransmit(SprintIR, get_readigns);
      uartReceive(COAF);                        //receiving replys from
      uartReceive(LOX_O2);
      uartReceive(SprintIR);
      //interpreting and spliting measurements in variables
      memcpy(co2_uf,   getMeasures(receive[SprintIR],'z', 5), 5);
      memcpy(co2_f,    getMeasures(receive[SprintIR],'Z', 5), 5);
      memcpy(co_uf,    getMeasures(receive[COAF],    'z', 5), 5);
      memcpy(co_f,     getMeasures(receive[COAF],    'Z', 5), 5);
      memcpy(o2_ppm,   getMeasures(receive[LOX_O2],  'O', 6), 6);
      memcpy(o2_xcent, getMeasures(receive[LOX_O2],  '%', 6), 6);
      memcpy(co_temp,  getMeasures(receive[COAF],    'T', 5), 5);
      memcpy(o2_temp,  getMeasures(receive[LOX_O2],  'T', 5), 5);
      memcpy(co_press, getMeasures(receive[COAF],    'B', 5), 5);
      memcpy(o2_press, getMeasures(receive[LOX_O2],  'P', 4), 4);
      memcpy(co_relH,  getMeasures(receive[COAF],    'H', 5), 5);
      //saving formated measurements in string DATA
      sprintf(DATA,
              "%d\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
              (int)t, co2_uf, co2_f, co_uf, co_f, o2_ppm, o2_xcent,
              co_temp, o2_temp, co_press, o2_press, co_relH);
      printf("\r%s", DATA);                     //showing measurements on display

      dfp = fopen(data_file_path, "a");         // open file for writing
      fprintf(dfp, "%s\n", DATA);               // saving measurements string to data file
      fclose(dfp);                              // close the file using the file pointer
      next_samp_time += sp_s;                   //adding sampling period to next_samp_time
    end_t = clock();                            //saving end time
    //calculate time to sleep
    inactivity_time =  iteration_time_ms - ((double)(end_t - start_t)*1e6 / CLOCKS_PER_SEC);
  }

  return 0;
}

char *getMeasures(char src[], char fval, int nchar)
{
	char * ptr = &src[0];
	static char s[10]="";

	while(*ptr != fval) ptr++;
	ptr += 2;
	memcpy(s, ptr, nchar);
	return s;
}

#include "daq.h"

int main(int argc, char *argv[]){

    int T_acq, t_samp;

    if(argc!=3){
        printf("Invalid number of arguments,\n"\
        "Usage: daq <daq duration(h)> <sampling period(s)>\nexiting!\n");
        return -2;
    }

    // configuring sensors
    printf("Configuring CO2 sensor\n");
    sensConf(SprintIR, B9600, Pulling_Mode, " K 00002\r\n", 5);
    sensConf(SprintIR, B9600, FILnUNFIL, " M 00006\r\n", 5);
    printf("Configuring CO sensor\n");
    sensConf(COAF, B9600, Pulling_Mode, "K 00002\r\n", 5);
    sensConf(COAF, B9600, M_zZTHB, "M 14406\r\n", 5);
    printf("Configuring O2 sensor\n");
    sensConf(LOX_O2, B9600, OX_P_Mode, "M 01\r\n", 5);

    // acquiring
    T_acq = atoi(argv[1]);//str to int
    t_samp = atoi(argv[2]);
    printf("Starting data acquisition with duration of %dh every %ds\n", T_acq, t_samp);
    DAQ(T_acq, t_samp);

    // deinit uart
    uartClose(SprintIR);
    uartClose(COAF);
    uartClose(LOX_O2);
    // finishing
    printf("Exiting of the program...\n");
    return 0;
}

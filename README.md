# DAQ System with Beaglebone Black using serial communication

This repositori contains a DAQ program and its C code source to implement a DAQ system with a Beaglebone Black using serial communication with sensors.
Sensors used in this system are: GC-0017 (Cabron dioxide), CM-31910 (Carbon monoxide, and temperature) and OX-0052 (Oxigen).

### Installing

To get this repositore you just need to clone it into your Beaglebone Black

```
git clone https://gitlab.com/sayeth.rd/daq-serial-bbb/
```

### Utilization

Configure UART1, UART2 and UART4 ports with this command
```
config-uart-pins.sh
```

Connect sensors to power supply and Beaglebone like next diagram.
![Diagrama conexiones](/images/conexion_sensores.png)

Then you can run the program wih this comand*:

```
./daq <daq duration(h)> <sampling period(s)>
```
*replace **<daq duration(h)>** with the total data acquisition time (in hours), and **<sampling period(s)>** with the time between each measurements acquisition (in seconds).

If you need to recompile the progam you can use **gcc** program from the terminal.
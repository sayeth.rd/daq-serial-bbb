// Guarding macro start
#ifndef UART_H
#define UART_H

// Includes begin
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <time.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdint.h>

// Error codes and return values
#define UART_FUNCTION_SUCCESSFUL        0
#define UART_NUMBER_INCORRECT           1
/*#define UART_BAUDRATE_INCORRECT         2
#define UART_FIFO_ERROR                 3
#define UART_INCORRECT_PATH             4*/

//VARIABLES
int uartFile[6];                    //file descriptor
unsigned char receive[6][100];      //declare a buffer for receiving data

// Function declarations
int uartConf(uint8_t uartNumber, int baudRate);
int uartClose(uint8_t uartNumber);
int uartTransmit(uint8_t uartNumber, unsigned char message[]);
int uartReceive(uint8_t uartNumber);

// Guarding macro end
#endif // UART_H

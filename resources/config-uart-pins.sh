#!/bin/bash
#uart1
config-pin P9_24 uart
config-pin -q P9_24
config-pin P9_26 uart
config-pin -q P9_26

#uart2
config-pin P9_21 uart
config-pin -q P9_21
config-pin P9_22 uart
config-pin -q P9_22

#uart4
config-pin P9_13 uart
config-pin -q P9_13
config-pin P9_11 uart
config-pin -q P9_11